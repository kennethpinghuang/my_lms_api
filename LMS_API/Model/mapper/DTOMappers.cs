﻿using System;
using System.Collections.Generic;
using LMS_API.Model.DTOs;

namespace LMS_API.Model.mapper
{
    public static class DTOMappers
    {
        public static EnrollmentDTO DTOMapper(this Enrollment enrollment)
        {
            return new EnrollmentDTO
            {
                CourseId = enrollment.CourseId,
                StudentId = enrollment.StudentId,
                Course = enrollment.Course.DTOMapper()
            };
        }

        public static ICollection<EnrollmentDTO> DTOMapper(this ICollection<Enrollment> enrollments){
            List< EnrollmentDTO > enrollmentDTOs = new List<EnrollmentDTO>();
            foreach (var en in enrollments)
            {
                enrollmentDTOs.Add(en.DTOMapper());
            }
            return enrollmentDTOs;
        }

        public static EnrollmentPostDelDTO EnrollmentBasicDTOMapper(this Enrollment enrollment)
        {
            return new EnrollmentPostDelDTO
            {
                CourseId = enrollment.CourseId,
                StudentId = enrollment.StudentId

            };
        }

        public static ICollection<EnrollmentPostDelDTO> EnrollmentBasicDTOMapper(this ICollection<Enrollment> enrollments){
            List<EnrollmentPostDelDTO> enrollmentPostDelDTOs = new List<EnrollmentPostDelDTO>();
            foreach (var en in enrollments){
                enrollmentPostDelDTOs.Add(en.EnrollmentBasicDTOMapper());
            }
            return enrollmentPostDelDTOs;
        }

        public static StudentDTO DTOMapper(this Student student){
            return new StudentDTO
            {
                Id = student.Id,
                FirstName = student.FirstName,
                LastName = student.LastName,
                CreditLimit = student.CreditLimit,
                Enrollments = student.Enrollments.DTOMapper()
            };
        }

        public static StudentBasicDTO StudentBasicDTOMapper(this Student student){
            return new StudentBasicDTO
            {
                Id = student.Id,
                FirstName = student.FirstName,
                LastName = student.LastName,
                CreditLimit = student.CreditLimit

            };   
        }

        public static CourseBasicDTO DTOMapper(this Course course){
            return new CourseBasicDTO
            {
                CourseId = course.CourseId,
                Title = course.Title,
                Credit = course.Credit,
                MaxStudentNum = course.MaxStudentNum
            };
        }

        public static CourseDTO CourseFullDTOMapper(this Course course){  //called by CourseController
            return new CourseDTO
            {
                CourseId = course.CourseId,
                Title = course.Title,
                Credit = course.Credit,
                MaxStudentNum = course.MaxStudentNum,
                Teachings = course.Teachings.DTOMapper(),
                Enrollments = course.Enrollments.EnrollmentBasicDTOMapper()
            };
        }

        public static TeachingDTO DTOMapper(this Teaching teaching){  //called by ICollection<TeachingDTO> DTOMapper
            return new TeachingDTO
            {
                LecturerId = teaching.LecturerId,
                CourseId = teaching.CourseId,
                Lecturer = teaching.Lecturer.LecturerBasicDTOMapper()
            };
        }

        public static ICollection<TeachingDTO> DTOMapper(this ICollection<Teaching> TeachingList){// called by courseDTO
            List<TeachingDTO> teachingDTOs = new List<TeachingDTO>();
            foreach (var teaching in TeachingList){
                teachingDTOs.Add(teaching.DTOMapper());
            }
            return teachingDTOs;
        }

        public static LecturerBasicDTO LecturerBasicDTOMapper(this Lecturer lecturer){  //called by TeachingDTO DTOMapper()
            return new LecturerBasicDTO
            {
                LecturerId = lecturer.LecturerId,
                FirstName = lecturer.FirstName,
                LastName = lecturer.LastName
            };
        }

        public static LecturerDTO DTOMapper(this Lecturer lecturer)
        {
            return new LecturerDTO
            {
                LecturerId = lecturer.LecturerId,
                FirstName = lecturer.FirstName,
                LastName = lecturer.LastName,
                Teachings = lecturer.Teachings.TeachingWithCourseMapper()

            };
        }

        public static ICollection<TeachingWithCourseDTO> TeachingWithCourseMapper(this ICollection<Teaching> teachings){
            List<TeachingWithCourseDTO> teachingWithCourseDTOs = new List<TeachingWithCourseDTO>();
            foreach(var teaching in teachings){
                teachingWithCourseDTOs.Add(teaching.TeachingWithCourseMapper());
            }
            return teachingWithCourseDTOs;
        }

        public static TeachingWithCourseDTO TeachingWithCourseMapper(this Teaching teaching)
        {
            return new TeachingWithCourseDTO
            {
                LecturerId = teaching.LecturerId,
                CourseId = teaching.CourseId,
                Course = teaching.Course.DTOMapper()
            };
        }

    }
}
