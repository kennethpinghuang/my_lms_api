﻿using System;
using System.Threading.Tasks;
using LMS_API.Model.DTOs;

namespace LMS_API.Model
{
    public class EnrollmentDTO
    {
        public int StudentId;
        public int CourseId;
       
        public CourseBasicDTO Course { get; set; }
    }

}
