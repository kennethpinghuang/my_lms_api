﻿using System;
using System.Collections.Generic;
namespace LMS_API.Model.DTOs
{
    public class LecturerDTO
    {
        public int LecturerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public ICollection<TeachingWithCourseDTO> Teachings { get; set; }
    }
}
