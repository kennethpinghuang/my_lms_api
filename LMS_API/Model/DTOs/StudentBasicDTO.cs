﻿using System;
namespace LMS_API.Model.DTOs
{
    public class StudentBasicDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int CreditLimit { get; set; }
    }
}
