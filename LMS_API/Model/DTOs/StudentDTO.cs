﻿using System;
using System.Collections.Generic;
using LMS_API.Model.DTOs;
namespace LMS_API.Model
{
    public class StudentDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int CreditLimit { get; set; }

        public ICollection<EnrollmentDTO> Enrollments { get; set; }
    }
}
