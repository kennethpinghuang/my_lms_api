﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS_API.Model.DTOs;

namespace LMS_API.Model
{
    public class TeachingDTO
    {
        public int LecturerId;
        public int CourseId;

        public LecturerBasicDTO Lecturer { get; set; }
    }
}
