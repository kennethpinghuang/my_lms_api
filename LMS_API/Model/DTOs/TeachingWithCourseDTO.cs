﻿using System;
namespace LMS_API.Model.DTOs
{
    public class TeachingWithCourseDTO
    {
        public int LecturerId { get; set; }
        public int CourseId { get; set; }

        public CourseBasicDTO Course { get; set; }
    }
}
