﻿using System;
namespace LMS_API.Model.DTOs
{
    public class LecturerBasicDTO
    {
        public int LecturerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

    }
}
