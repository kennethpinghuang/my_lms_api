﻿using System;

namespace LMS_API.Model.DTOs
{
    public class TeachingPostDelDTO
    {
        public int LecturerId;
        public int CourseId;
    }
}
