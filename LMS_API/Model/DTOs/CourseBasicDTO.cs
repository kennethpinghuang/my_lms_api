﻿using System;
using System.Collections.Generic;


namespace LMS_API.Model
{
    public class CourseBasicDTO
    {
        public int CourseId { get; set; }
        public string Title { get; set; }
        public int Credit { get; set; }
        public int MaxStudentNum { get; set; }
    }

}

