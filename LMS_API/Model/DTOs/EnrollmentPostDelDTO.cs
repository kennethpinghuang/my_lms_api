﻿using System;
namespace LMS_API.Model.DTOs
{
    public class EnrollmentPostDelDTO
    {
        public int StudentId;
        public int CourseId;
    }
}
