﻿using System;
using System.Collections.Generic;

namespace LMS_API.Model.DTOs
{
    public class CourseDTO
    {
        public int CourseId { get; set; }
        public string Title { get; set; }
        public int Credit { get; set; }
        public int MaxStudentNum { get; set; }


        public ICollection<TeachingDTO> Teachings { get; set; }
        public ICollection<EnrollmentPostDelDTO> Enrollments { get; set; }

    }
}
